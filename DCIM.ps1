 $wakeOnLan = Get-WmiObject -Namespace Root\DCIM\SYSMAN -Class DCIM_BIOSEnumeration | Where-object {$_.AttributeName -eq 'Wake-On-LAN'}
if ($wakeOnLan.PossibleValues -contains $wakeOnLan.CurrentValue){  
    $index = (0..($wakeOnLan.PossibleValues).Count) | Where-Object {$wakeOnLan.PossibleValues[$_] -eq $wakeOnLan.CurrentValue}
}

$autoOnEnabled = Get-WmiObject -Namespace Root\DCIM\SYSMAN -Class DCIM_BIOSEnumeration | Where-object {$_.AttributeName -eq 'Auto On'}
if ($autoOnEnabled.PossibleValues -contains $autoOnEnabled.CurrentValue){  
    $index = (0..($autoOnEnabled.PossibleValues).Count) | Where-Object {$autoOnEnabled.PossibleValues[$_] -eq $autoOnEnabled.CurrentValue}
}

$autoOnMinute = Get-WmiObject -Namespace Root\DCIM\SYSMAN -Class DCIM_BIOSEnumeration | Where-object {$_.AttributeName -eq 'Auto On Minute'}

$autoOnHour = Get-WmiObject -Namespace Root\DCIM\SYSMAN -Class DCIM_BIOSEnumeration | Where-object {$_.AttributeName -eq 'Auto On Hour'}

$wakeOnLanReadableString = "Wake On Lan Status:" + $wakeOnLan.PossibleValuesDescription[$index]
$autoOnEnabledReadableString = "Auto On Status:" + $wakeOnLan.PossibleValuesDescription[$index]
$autoOnTimeReadableString = "Computer will wake up at:" + $autoOnHour.CurrentValue + ":" + $autoOnMinte.CurrentValue

$name = (Get-Item Env:\COMPUTERNAME).value
$filepath = (Get-ChildItem Env:\USERPROFILE).value


-join $wakeOnLanReadableString, $autoOnEnabledReadableString, $autoOnTimeReadableString | Out-File "$filepath\$name biosSettings.txt"